# Home Office Test - Candidate 2243868
## Check UK VISA site - web test

# Overview:
The technical assessment is part of the interview and will be used to assess a candidate’s ability to setup their development/test environment, pull in the dependencies of an existing framework (serenity) from scratch using Maven, create new automated tests and communicate their work and results.


# Technical Requirements:
You will be expected to know the basics of source control/version control using Git, Java, Maven as a build tool and open source test automation libraries - Serenity (cucumber framework) and Selenium Webdriver).


# Assessment:
This assessment will require you to create your own project on GitLab, called “Candidate 2243868”. If you are not yet registered on GitLab, please follow the link below: https://gitlab.com/users/sign_in#register-pane

During this time you will need to create a new public GitLab project. You will be expected to use Maven to pull in Serenity dependencies – there are some good example cucumber Serenity POM files on GitHub.

Please create a feature branch and work from this, when you have completed all scenarios, raise a pull request to Master branch for review.

You will be expected to write the step definitions, page objects and any support code in Java. The results of the review will be the focus of the first 15 minutes of the interview where you will be expected to discuss your submission and talk through how you approached the solution.


# Project:
You are expected to create your own public GitLab project which will be named after your < Candidate ID > (Candidate 2243868). 
You will create and work off a feature branch, once you have completed the assessment, raise a pull request for your work to be reviewed

Below are some guidelines on the technologies we expect you to be using for this assessment.

Serenity (Cucumber framework)
Maven
Rest Assured or Serenity Rest
Assertion library of your choice
 
You may include a Readme.txt file within your submission to provide any details on your submission

# Scenarios:
Application under test: Check UK visa website; URL: https://www.gov.uk/check-uk-visa/y

Feature – Confirm whether a visa is required to visit the UK
Scenarios:
Given I provide a nationality of Japan
And I select the reason “Study”
And I state I am intending to stay for more than 6 months
When I submit the form
Then I will be informed “I need a visa to study in the UK”


Given I provide a nationality of Japan
And I select the reason “Tourism”
When I submit the form
Then I will be informed “I won’t need a visa to study in the UK”


Given I provide a nationality of Russia
And I select the reason “Tourism”
And I state I am not travelling or visiting a partner or family
When I submit the form
Then I will be informed “I need a visa to come to the UK”

 
Application under test: Postcodes Api

Feature –  Query a postcode and receive a 200 response
Scenarios:
When I send a get request to api.postcodes.io/postcodes/SW1P4JA
Then I get a 200 response


Please note that the instructions above are for guidance only – you are trying to achieve the specific features of:
Confirm whether a visa is required to visit the UK for:
Japan and Study
Japan and Tourism
Russia and Tourism
Feature –  Query a postcode and receive a 200 response
 
If that requires that you must adapt the feature file from the basic guidance that has been given above, you should make the required changes to achieve the desired results – you can supply a readme.txt file as per the above if you wish to provide further details of the actions you have taken and why.


# Solution:
## Notes:
* My tests are running fine from within IntelliJ. I can run the individual Scenarios or run the entire Feature
* I have had an issue configuring the runner file and the Serenity reports.  
* These were the problems:
	* When i run the feature/scenarios from within IntelliJ they all run fine 
		* It doesn't create a Serenity Report though.  
		* I am getting the JUnit report.		
	* When I run from maven (mvn - verify), it appears to be fine
		* It creates the Serenity Report but none of the tests are executed, so the report has no stats
    * When I run the Test Runner directly I get an error message: Caused by: java.lang.ClassNotFoundException: io.cucumber.java.PendingException

# Features:
* I have used Scenario outlines with examples, as i thought it would make the steps a little more reusable.
	* It also allowed me to run a number of different combinations of data to increase coverage.
	* Especially when their lists of selections as I always like to choose the first and last and onw in between
	* I also liked to choose names where there are unusual characters, such as foreign spellings
* Although I have tried to clean up my Step code, I would have liked to refactor it a bit more.
* On the Features and Scenarios: I have used Scenario Outlines and Examples to repeat the same scenario many times with different combinations.  This allows a greater test coverage by using all the inventory in each test. The weigh up is it can look a little confusing at times, so it might need cleaning up to make it read better.

# Setting up:
## Project
* Clone this repository to your local machine in the usual way e.g. using HTTPS:
    > git clone https://github.com/wab66/Candidate-2243868

## Maven build
I have included my POM file

## IDE option
1. Open the project from your IDE - e.g. IntellijJ IDEA
2. Navigate to /src/test/resources/features/...
3. There are two main ways to run the tests:
    * Run test from the Project Explorer
    * Right click on the feature 
    * Select Run Feature (green play button)
4. Open the Feature file
    * Inside the line number grid on the left you can click on the green play button, from the Feature heading - this will run the entire feature
    * Or if you would like to run an individual Scenario instead you can scroll down to the required scenario and choose the green play button associated to that scenario
5. The tests are currently set to run in headless mode - I did run it without headless mode just to see it working
		5.1 To turn headless mode off/on - go to '/src/test/resources/serenity.conf' file and change 'headless.mode = true or false'

## View the test results
* From the 4. Run tab that has been opened by IntelliJ in the left-hand panel, there will be a Test Results section
  * In this section if you click on the 'Export Test Results' icon (that looks like an L with an arrow pointing up), which is in the top right section
  * This will generate a HTML report in the root of your project: Test Results - Feature name.html
    * Double click the report and it will be opened in the main window
      * when you mouse over the html code you will get a floating list of Browser icons, which you can click to open the html file in a browser

 
 
